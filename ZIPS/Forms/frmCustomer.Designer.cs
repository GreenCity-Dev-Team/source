﻿namespace ZIPS.Forms
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.txtPostCode = new System.Windows.Forms.TextBox();
            this.grpInvoiceDay = new System.Windows.Forms.GroupBox();
            this.rbSunday = new System.Windows.Forms.RadioButton();
            this.rbSaturday = new System.Windows.Forms.RadioButton();
            this.rbFriday = new System.Windows.Forms.RadioButton();
            this.rbThursday = new System.Windows.Forms.RadioButton();
            this.rbWednesday = new System.Windows.Forms.RadioButton();
            this.rbTuesday = new System.Windows.Forms.RadioButton();
            this.rbMonday = new System.Windows.Forms.RadioButton();
            this.txtInvoiceText = new System.Windows.Forms.TextBox();
            this.txtTerms = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.grpInvoiceDay.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnSave);
            this.pnlFooter.Controls.Add(this.btnCancel);
            this.pnlFooter.Location = new System.Drawing.Point(0, 253);
            this.pnlFooter.Size = new System.Drawing.Size(874, 44);
            this.pnlFooter.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Town";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Post Code";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(418, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Invoice Day";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(418, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Invoice Text";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 16);
            this.label8.TabIndex = 5;
            this.label8.Text = "Terms";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(117, 9);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(100, 23);
            this.txtCode.TabIndex = 0;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(117, 39);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(259, 23);
            this.txtName.TabIndex = 1;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(117, 68);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(259, 23);
            this.txtAddress1.TabIndex = 2;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(117, 97);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(259, 23);
            this.txtAddress2.TabIndex = 3;
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(117, 126);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(259, 23);
            this.txtTown.TabIndex = 4;
            // 
            // txtPostCode
            // 
            this.txtPostCode.Location = new System.Drawing.Point(117, 155);
            this.txtPostCode.Name = "txtPostCode";
            this.txtPostCode.Size = new System.Drawing.Size(100, 23);
            this.txtPostCode.TabIndex = 5;
            // 
            // grpInvoiceDay
            // 
            this.grpInvoiceDay.Controls.Add(this.rbSunday);
            this.grpInvoiceDay.Controls.Add(this.rbSaturday);
            this.grpInvoiceDay.Controls.Add(this.rbFriday);
            this.grpInvoiceDay.Controls.Add(this.rbThursday);
            this.grpInvoiceDay.Controls.Add(this.rbWednesday);
            this.grpInvoiceDay.Controls.Add(this.rbTuesday);
            this.grpInvoiceDay.Controls.Add(this.rbMonday);
            this.grpInvoiceDay.Location = new System.Drawing.Point(513, 0);
            this.grpInvoiceDay.Name = "grpInvoiceDay";
            this.grpInvoiceDay.Size = new System.Drawing.Size(215, 120);
            this.grpInvoiceDay.TabIndex = 7;
            this.grpInvoiceDay.TabStop = false;
            // 
            // rbSunday
            // 
            this.rbSunday.AutoSize = true;
            this.rbSunday.Location = new System.Drawing.Point(116, 62);
            this.rbSunday.Name = "rbSunday";
            this.rbSunday.Size = new System.Drawing.Size(68, 20);
            this.rbSunday.TabIndex = 6;
            this.rbSunday.TabStop = true;
            this.rbSunday.Text = "Sunday";
            this.rbSunday.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.rbSunday.UseVisualStyleBackColor = true;
            // 
            // rbSaturday
            // 
            this.rbSaturday.AutoSize = true;
            this.rbSaturday.Location = new System.Drawing.Point(116, 37);
            this.rbSaturday.Name = "rbSaturday";
            this.rbSaturday.Size = new System.Drawing.Size(77, 20);
            this.rbSaturday.TabIndex = 5;
            this.rbSaturday.TabStop = true;
            this.rbSaturday.Text = "Saturday";
            this.rbSaturday.UseVisualStyleBackColor = true;
            // 
            // rbFriday
            // 
            this.rbFriday.AutoSize = true;
            this.rbFriday.Location = new System.Drawing.Point(116, 12);
            this.rbFriday.Name = "rbFriday";
            this.rbFriday.Size = new System.Drawing.Size(61, 20);
            this.rbFriday.TabIndex = 4;
            this.rbFriday.TabStop = true;
            this.rbFriday.Text = "Friday";
            this.rbFriday.UseVisualStyleBackColor = true;
            // 
            // rbThursday
            // 
            this.rbThursday.AutoSize = true;
            this.rbThursday.Location = new System.Drawing.Point(7, 87);
            this.rbThursday.Name = "rbThursday";
            this.rbThursday.Size = new System.Drawing.Size(79, 20);
            this.rbThursday.TabIndex = 3;
            this.rbThursday.TabStop = true;
            this.rbThursday.Text = "Thursday";
            this.rbThursday.UseVisualStyleBackColor = true;
            // 
            // rbWednesday
            // 
            this.rbWednesday.AutoSize = true;
            this.rbWednesday.Location = new System.Drawing.Point(7, 62);
            this.rbWednesday.Name = "rbWednesday";
            this.rbWednesday.Size = new System.Drawing.Size(92, 20);
            this.rbWednesday.TabIndex = 2;
            this.rbWednesday.TabStop = true;
            this.rbWednesday.Text = "Wednesday";
            this.rbWednesday.UseVisualStyleBackColor = true;
            // 
            // rbTuesday
            // 
            this.rbTuesday.AutoSize = true;
            this.rbTuesday.Location = new System.Drawing.Point(7, 37);
            this.rbTuesday.Name = "rbTuesday";
            this.rbTuesday.Size = new System.Drawing.Size(74, 20);
            this.rbTuesday.TabIndex = 1;
            this.rbTuesday.TabStop = true;
            this.rbTuesday.Text = "Tuesday";
            this.rbTuesday.UseVisualStyleBackColor = true;
            // 
            // rbMonday
            // 
            this.rbMonday.AutoSize = true;
            this.rbMonday.Location = new System.Drawing.Point(7, 12);
            this.rbMonday.Name = "rbMonday";
            this.rbMonday.Size = new System.Drawing.Size(70, 20);
            this.rbMonday.TabIndex = 0;
            this.rbMonday.TabStop = true;
            this.rbMonday.Text = "Monday";
            this.rbMonday.UseVisualStyleBackColor = true;
            // 
            // txtInvoiceText
            // 
            this.txtInvoiceText.Location = new System.Drawing.Point(513, 130);
            this.txtInvoiceText.Name = "txtInvoiceText";
            this.txtInvoiceText.Size = new System.Drawing.Size(334, 23);
            this.txtInvoiceText.TabIndex = 8;
            // 
            // txtTerms
            // 
            this.txtTerms.Location = new System.Drawing.Point(117, 184);
            this.txtTerms.Name = "txtTerms";
            this.txtTerms.Size = new System.Drawing.Size(730, 23);
            this.txtTerms.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(400, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 31);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(787, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 31);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(874, 297);
            this.ControlBox = false;
            this.Controls.Add(this.txtTerms);
            this.Controls.Add(this.txtInvoiceText);
            this.Controls.Add(this.grpInvoiceDay);
            this.Controls.Add(this.txtPostCode);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.txtAddress2);
            this.Controls.Add(this.txtAddress1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "frmCustomer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.txtCode, 0);
            this.Controls.SetChildIndex(this.txtName, 0);
            this.Controls.SetChildIndex(this.txtAddress1, 0);
            this.Controls.SetChildIndex(this.txtAddress2, 0);
            this.Controls.SetChildIndex(this.txtTown, 0);
            this.Controls.SetChildIndex(this.txtPostCode, 0);
            this.Controls.SetChildIndex(this.grpInvoiceDay, 0);
            this.Controls.SetChildIndex(this.txtInvoiceText, 0);
            this.Controls.SetChildIndex(this.txtTerms, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.grpInvoiceDay.ResumeLayout(false);
            this.grpInvoiceDay.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.TextBox txtPostCode;
        private System.Windows.Forms.GroupBox grpInvoiceDay;
        private System.Windows.Forms.RadioButton rbSunday;
        private System.Windows.Forms.RadioButton rbSaturday;
        private System.Windows.Forms.RadioButton rbFriday;
        private System.Windows.Forms.RadioButton rbThursday;
        private System.Windows.Forms.RadioButton rbWednesday;
        private System.Windows.Forms.RadioButton rbTuesday;
        private System.Windows.Forms.RadioButton rbMonday;
        private System.Windows.Forms.TextBox txtInvoiceText;
        private System.Windows.Forms.TextBox txtTerms;
    }
}
