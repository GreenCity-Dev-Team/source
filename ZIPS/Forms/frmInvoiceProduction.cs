﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ZIPS.Classes;
using ZIPS.Data;
using System.Collections;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;

namespace ZIPS.Forms
{
    public partial class frmInvoiceProduction : frmBase
    {
        private SortableBindingList<Data.Customer> CustomerList;
        int NextInvoiceNo = 0;

        public frmInvoiceProduction()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void frmInvoiceProduction_Load(object sender, EventArgs e)
        {
            loading = true;

            lblVersionText.Text = string.Format("Version: {0}", Assembly.GetEntryAssembly().GetName().Version.ToString());

            Functions.FormatButton(btnCSV);
            Functions.FormatButton(btnClose);
            Functions.FormatButton(btnImportBalances);
            Functions.FormatButton(btnNextWeek);
            Functions.FormatButton(btnPrintDriverCustomer);
            Functions.FormatButton(btnPrintOffice);
            Functions.FormatButton(btnSelectAll);
            Functions.FormatButton(btnSelectNone);
            Functions.StyleDataGridView(dgvInvoices);

            rbPrepare.Checked = (Properties.Settings.Default.Status == "Prepare");
            rbReady.Checked = !rbPrepare.Checked;

            rbCurrent.Checked = (Properties.Settings.Default.Year == "C");
            rbNext.Checked = !rbCurrent.Checked;

            SetFieldsFromCompanyData();

            LoadGrid();
            FormatForStatus();
            loading = false;
        }

        private void SetFieldsFromCompanyData()
        {
            nudMonth.Value = Properties.Settings.Default.InvoiceMonday.Month;

            lblMondayDate.Text = Properties.Settings.Default.InvoiceMonday.ToShortDateString();
            lblInvoiceNo.Text = Properties.Settings.Default.NextInvoiceNo.ToString();
            NextInvoiceNo = Properties.Settings.Default.NextInvoiceNo;

        }

        private void FormatForStatus()
        {
            bool blnReady = rbReady.Checked;

            rbCurrent.Enabled = !blnReady;
            rbNext.Enabled = !blnReady;
            nudMonth.Enabled = !blnReady;
            btnImportBalances.Enabled = !blnReady;
            btnPrintOffice.Enabled = blnReady;
            btnPrintDriverCustomer.Enabled = blnReady;
            btnCSV.Enabled = blnReady;
            btnNextWeek.Enabled = blnReady;
            btnSelectAll.Enabled = !blnReady;
            btnSelectNone.Enabled = !blnReady;
            dgvInvoices.ReadOnly = blnReady;
            grpSort.Visible = blnReady;
        }

        private void rbCurrentNextChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                //Ensure code below only run once
                RadioButton senderRadioButton = sender as RadioButton;
                if (senderRadioButton.Checked)
                {
                    if (rbCurrent.Checked)
                    {
                        Properties.Settings.Default.Year = "C";
                    }
                    else
                    {
                        Properties.Settings.Default.Year = "N";
                    }
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void rbInvoiceSortChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                AssignInvoiceNumbers();
                LoadGrid();
            }
        }

        private void rbPrepareReadyChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                //Ensure code below only run once
                RadioButton senderRadioButton = sender as RadioButton;
                if (senderRadioButton.Checked)
                {
                    if (rbReady.Checked)
                        Properties.Settings.Default.Status = "Ready";
                    else
                        Properties.Settings.Default.Status = "Prepare";

                    Properties.Settings.Default.Save();
                    //If ready then assign invoice numbers and dates to those marked for inclusion and save the records
                    //Redraw the grid without the include column, only including those with the include set and showing the
                    //invoice dates and numbers
                    if (rbReady.Checked)
                    {
                        //First save all the includes back as we need to know what's included before we dish out the invoice numbers
                        foreach (DataGridViewRow r in dgvInvoices.Rows)
                        {
                            Customer c = r.DataBoundItem as Customer;
                            CustomerDAL.UpdateCustomer(c);
                        }
                        AssignInvoiceNumbers();

                        dgvInvoices.Columns["InvoiceNoColumn"].Visible = true;
                        dgvInvoices.Columns["InvoiceDateColumn"].Visible = true;
                        dgvInvoices.Columns["InvoiceThisWeekColumn"].Visible = false;
                    }
                    else
                    {
                        foreach (DataGridViewRow r in dgvInvoices.Rows)
                        {
                            Customer c = r.DataBoundItem as Customer;
                            c.InvoiceThisWeek = false;
                            CustomerDAL.UpdateCustomer(c);
                        }
                        dgvInvoices.Columns["InvoiceNoColumn"].Visible = false;
                        dgvInvoices.Columns["InvoiceDateColumn"].Visible = false;
                        dgvInvoices.Columns["InvoiceThisWeekColumn"].Visible = true;
                        NextInvoiceNo = int.Parse(lblInvoiceNo.Text);
                    }
                    LoadGrid();
                    FormatForStatus();
                }
            }
        }

        private void AssignInvoiceNumbers()
        {
            //this only returns accounts that have been marked for inclusion
            List<Customer> sortedCustomerList = CustomerDAL.GetSortedCustomerList(rbDateAndAccount.Checked);
            int InvoiceNo = Properties.Settings.Default.NextInvoiceNo;
            DateTime MondayDate = Properties.Settings.Default.InvoiceMonday;
            foreach (Customer c in sortedCustomerList)
            {
                c.InvoiceNo = InvoiceNo;
                InvoiceNo += 1;
                c.InvoiceDate = MondayDate.AddDays(c.InvoiceOffset);
                CustomerDAL.UpdateCustomer(c);
            }
            NextInvoiceNo = InvoiceNo;
        }

        private void LoadGrid(int selectedId = 0)
        {
            int intCurrTopRow = dgvInvoices.FirstDisplayedScrollingRowIndex;
            if (intCurrTopRow < 0)
            {
                intCurrTopRow = 0;
            }
            if (rbPrepare.Checked)
            {
                CustomerList = CustomerDAL.GetCustomerList();
                dgvInvoices.DataSource = CustomerList;
                if (selectedId == 0)
                {
                    dgvInvoices.Sort(dgvInvoices.Columns["NameColumn"], ListSortDirection.Ascending);
                }
            }
            else
            {
                dgvInvoices.DataSource = CustomerDAL.GetSortedCustomerList(rbDateAndAccount.Checked);
            }
            

            int intRowOfSelItem = 0;
            if (selectedId != 0)
            {
                dgvInvoices.FirstDisplayedScrollingRowIndex = intCurrTopRow;
                foreach (DataGridViewRow r in dgvInvoices.Rows)
                {
                    if ((int)r.Cells["Id"].Value == selectedId)
                    {
                        intRowOfSelItem = r.Index;
                    }
                }
                dgvInvoices.CurrentCell = dgvInvoices.Rows[intRowOfSelItem].Cells["NameColumn"];
                dgvInvoices.Rows[intRowOfSelItem].Selected = true;
            }

        }

        private void btnImportBalances_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.LastImportFileLocation))
                dlgOpenFile.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            else
                dlgOpenFile.InitialDirectory = Properties.Settings.Default.LastImportFileLocation;

            dlgOpenFile.Filter = "CSV Files| *.csv";
            dlgOpenFile.Multiselect = false;
            dlgOpenFile.FileName = string.Empty;

            DialogResult result = dlgOpenFile.ShowDialog();
            if (result == DialogResult.OK)
            {
                string fileName = dlgOpenFile.FileName;
                Properties.Settings.Default.LastImportFileLocation = Path.GetDirectoryName(dlgOpenFile.FileName);
                Properties.Settings.Default.Save();

                //Clear current balances first
                foreach (Customer c in CustomerList)
                {
                    if (c.OutstandingBalance > 0)
                    {
                        c.OutstandingBalance = 0;
                        CustomerDAL.UpdateCustomer(c);
                    }
                }

                //Do the import:
                List<string[]> csvData = new List<string[]>();
                string line;
                string[] row;
                int rowsAffected = 0;
                string strBalance;
                decimal decBalance;
                try
                {
                    using (StreamReader r = new StreamReader(fileName))
                    {
                        while ((line = r.ReadLine()) != null)
                        {
                            row = line.Split(',');
                            csvData.Add(row);
                        }
                    }
                    //File should have account code in first column and a balance figure in the second
                    for (int i = 0; i < csvData.Count; i++)
                    {
                        if ((csvData[i][0].ToString() ?? string.Empty) != string.Empty)
                        {
                            Customer c = CustomerDAL.GetCustomer(csvData[i][0].ToString());
                            if (c != null)
                            {
                                strBalance = csvData[i][1].ToString();
                                if (decimal.TryParse(strBalance, out decBalance))
                                {
                                    c.OutstandingBalance = decBalance;
                                    CustomerDAL.UpdateCustomer(c);
                                    rowsAffected += 1;
                                }
                            }
                        }
                    }
                    if (rowsAffected == 0)
                    {
                        MessageBox.Show("No balances were imported. Please check the format of your file. It should have two columns:\n - Account Number\n - Balance (without currency symbol",
                            Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        MessageBox.Show(string.Format("{0} balances imported", rowsAffected), Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadGrid();
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "frmInvoiceProduction_btnImportBalances_Click", true);
                }

            }

        }

        private void btnNextWeek_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will move you on to the next invoicing week. Please make sure you have printed the invoices and created the CSV file before continuing. Do you want to continue?",
                Properties.Settings.Default.ShortSystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                Properties.Settings.Default.NextInvoiceNo = NextInvoiceNo;
                Properties.Settings.Default.InvoiceMonday = Properties.Settings.Default.InvoiceMonday.AddDays(7);
                Properties.Settings.Default.Save();
                lblMondayDate.Text = Properties.Settings.Default.InvoiceMonday.ToShortDateString();
                lblInvoiceNo.Text = Properties.Settings.Default.NextInvoiceNo.ToString();
                rbPrepare.Checked = true;
            }
        }

        private void companyDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCompany frmCompanyData = new frmCompany();
            frmCompanyData.openingForm = this;
            frmCompanyData.ShowDialog();
            SetFieldsFromCompanyData();
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCustomerList frm = new frmCustomerList();
            frm.openingForm = this;
            frm.ShowDialog();
            //Ensure any changes are reflected in grid
            LoadGrid();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmInvoiceProduction_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", Properties.Settings.Default.ShortSystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void btnCSV_Click(object sender, EventArgs e)
        {
            try 
	        {	        
		        if (string.IsNullOrEmpty(Properties.Settings.Default.CSVPath))
                {
                    MessageBox.Show("No CSV path has been set up. Please go into Company Data and select a path to store CSV exports.",
                        Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                string csvPath = Properties.Settings.Default.CSVPath;
                if (!Directory.Exists(csvPath))
                {
                    MessageBox.Show("The path set up for storing CSV exports cannot be found. Please go into Company Data and select a new path to store CSV exports.",
                        Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            
                CSVExport exportList = new CSVExport();
                decimal VATAmt;
                foreach (DataGridViewRow r in dgvInvoices.Rows)
                {
                    Customer c = r.DataBoundItem as Customer;
                    exportList.AddRow();
                    exportList["Transaction Type"] = Properties.Settings.Default.Transaction;
                    exportList["Customer Code"] = c.AccountNumber;
                    exportList["Delivery Terms"] = Properties.Settings.Default.DeliveryTerms;
                    exportList["Transaction Nature"] = "";
                    exportList["Transport Method"] = "";
                    exportList["Reference"] = string.Format("C{0}", c.InvoiceNo.ToString());
                    exportList["Date"] = c.InvoiceDate.ToShortDateString();
                    exportList["Due Date"] = (c.InvoiceDate.AddDays(Properties.Settings.Default.DueDate)).ToShortDateString();
                    exportList["Anticipated Receipt Date"] = (c.InvoiceDate.AddDays(Properties.Settings.Default.DueDate)).AddDays(Properties.Settings.Default.AnticipatedDate).ToShortDateString(); ;
                    exportList["Description"] = c.InvoiceText;
                    exportList["Currency Rate or Tri Rate 1"] = "";
                    exportList["Dispute"] = "";
                    exportList["Settlement Discount % 1"] = "";
                    exportList["Settlement Discount Days 1"] = "";
                    exportList["Settlement Discount % 2"] = "";
                    exportList["Settlement Discount Days 2"] = "";
                    exportList["Archive Flag"] = "";
                    exportList["Nett Amount"] = c.InvoiceAmt.ToString("0.00");
                    exportList["VAT Code"] = Properties.Settings.Default.VATCode;
                    VATAmt = c.InvoiceAmt * (Properties.Settings.Default.VATRate / 100);
                    exportList["VAT Amount"] = VATAmt.ToString("0.00");
                    exportList["Gross Amount"] = (c.InvoiceAmt + VATAmt).ToString("0.00");
                    exportList["Unallocated Amount"] = (c.InvoiceAmt + VATAmt).ToString("0.00");
                    exportList["Analysis Code"] = Properties.Settings.Default.AnalysisCode;
                    exportList["Project Code"] = "";
                    exportList["Cost Centre Code"] = "";
                    exportList["Detail Notes"] = Properties.Settings.Default.DetailedNote;
                    exportList["Year"] = Properties.Settings.Default.Year;
                    exportList["Period"] = Properties.Settings.Default.Period;
                    exportList["Batch Reference"] = Properties.Settings.Default.BatchRef;
                    exportList["Currency Code"] = Properties.Settings.Default.InvoiceCurrency;
                    exportList["Sub Ledger Code"] = "";
                    exportList["Transaction User Key 1"] = "";
                    exportList["Transaction User Key 2"] = "";
                    exportList["Transaction User Key 3"] = "";
                    exportList["2nd Base () Rate"] = "";
                    exportList["2nd Base () Net Amount"] = "";
                    exportList["2nd Base () VAT Amount"] = "";
                    exportList["2nd Base () Gross Amount"] = "";
                    exportList["Tri Rate 2"] = "";
                    exportList["Currency Net Amount"] = "";
                    exportList["Currency VAT Amount"] = "";
                    exportList["Currency Gross Amount"] = "";
                    exportList["Currency Unallocated Amount"] = "";
                }
                string fileName = string.Format("ZIPInvoices{0}.csv",DateTime.Today.ToString("ddMMyyyy"));
                string filePath = Path.Combine(Properties.Settings.Default.CSVPath,fileName);
                exportList.ExportToFile(filePath);
                MessageBox.Show(string.Format("CSV File created: {0}", filePath),Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
	        }
	        catch (Exception ex)
	        {
		        Exceptions.HandleException(ex, "frmInvoiceProduction_btnCSV_Click", true);
	        }
    }

        private void nudMonth_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Period = (int)nudMonth.Value;
            Properties.Settings.Default.Save();
        }

        private void btnPrintOffice_Click(object sender, EventArgs e)
        {
            ArrayList invoices = new ArrayList();
            foreach (DataGridViewRow r in dgvInvoices.Rows)
            {
                Customer c = r.DataBoundItem as Customer;
                Invoice inv = PopulateInvoice(c, "Invoice");
                invoices.Add(inv);
            }

            PrintInvoices(invoices);
            MessageBox.Show("Office Copy Invoices sent to your default printer", Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private Invoice PopulateInvoice(Customer c, string invTitle)
        {
            Invoice inv;
            decimal VATAmt;

            inv = new Invoice();
            inv.Title = invTitle;
            inv.AccountNumber = c.AccountNumber;
            inv.Name = c.Name;
            inv.Address1 = c.Address1;
            inv.Address2 = c.Address2;
            inv.Town = c.Town;
            inv.PostCode = c.PostCode;
            inv.InvoiceNo = c.InvoiceNo;
            inv.InvoiceDate = c.InvoiceDate;
            inv.InvoiceText = c.InvoiceText;
            inv.InvoiceAmt = c.InvoiceAmt;
            VATAmt = c.InvoiceAmt * (Properties.Settings.Default.VATRate / 100);
            inv.VAT = VATAmt;
            inv.Gross = inv.InvoiceAmt + VATAmt;
            if (c.OutstandingBalance != 0)
                inv.OutstandingBalance = c.OutstandingBalance + inv.Gross;
            else
                inv.OutstandingBalance = 0;
            inv.Terms = c.Terms;
            inv.CompanyAddress = Properties.Settings.Default.CompanyAddress;
            inv.CompanyTelNos = Properties.Settings.Default.CompanyTelNos;
            inv.VATRegNo = Properties.Settings.Default.VATRegNo;
            return inv;
        }

        private void PrintInvoices(ArrayList invoices)
        {
            ReportDocument invReport = new ReportDocument();
            invReport.Load(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),"ZIPS","rptInvoice.rpt"));
            invReport.SetDataSource(invoices);
            invReport.PrintToPrinter(1, false, 0, 0);
        }

        private void btnPrintDriverCustomer_Click(object sender, EventArgs e)
        {
            ArrayList invoices = new ArrayList();
            Invoice inv;
            foreach (DataGridViewRow r in dgvInvoices.Rows)
            {
                Customer c = r.DataBoundItem as Customer;
                inv = PopulateInvoice(c, "Invoice (Driver Copy)");
                invoices.Add(inv);
                inv = PopulateInvoice(c, "Invoice (Customer Copy)");
                invoices.Add(inv);
            }

            PrintInvoices(invoices);
            MessageBox.Show("Driver/Customer Invoices sent to your default printer", Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.LastBackupLocation))
                dlgBackup.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            else
                dlgBackup.InitialDirectory = Properties.Settings.Default.LastImportFileLocation;

            dlgBackup.Filter = "XML Files| *.xml";
            dlgBackup.FileName = string.Format("ZIPS_{0}.xml",DateTime.Today.ToString("ddMMyyyy"));
            dlgBackup.Title = "Choose path and name of backup file.";

            DialogResult result = dlgBackup.ShowDialog();
            if (result == DialogResult.OK)
            {
                //Copy xml data file to destination
                System.IO.File.Copy(Globals.dataFilePath, dlgBackup.FileName);
                MessageBox.Show("Backup completed", Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dgvInvoices_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (!loading)
            {
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    Customer c = dgvInvoices.Rows[e.RowIndex].DataBoundItem as Customer;
                    CustomerDAL.UpdateCustomer(c);
                }
            }
        }

        private void restoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.LastBackupLocation))
                dlgOpenFile.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            else
                dlgOpenFile.InitialDirectory = Properties.Settings.Default.LastImportFileLocation;

            dlgOpenFile.Filter = "XML Files| *.xml";
            dlgOpenFile.FileName = string.Empty;
            dlgOpenFile.Title = "Choose backup file to restore.";

            DialogResult result = dlgOpenFile.ShowDialog();
            if (result == DialogResult.OK)
            {
                //Copy xml data file from chosen file to data locatoin
                System.IO.File.Copy(dlgOpenFile.FileName,Globals.dataFilePath, true);
                LoadGrid();
                MessageBox.Show("Backup restored", Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvInvoices.Rows)
            {
                row.Cells["InvoiceThisWeekColumn"].Value = true;
            }
        }

        private void btnSelectNone_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvInvoices.Rows)
            {
                row.Cells["InvoiceThisWeekColumn"].Value = false;
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout frm = new frmAbout();
            frm.ShowDialog();
        }

        private void remoteSupportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Team Viewer will be downloaded onto your machine. When asked what action to perform on the download, click Run. " +
                "When the Team Viewer window appears, tell the software support engineer the 9 digit code and the password.",
                Properties.Settings.Default.ShortSystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);

            System.Diagnostics.Process.Start("http://www.totalsolutions.co.uk/downloads/TeamViewerQS.exe");
        }
    }
}
