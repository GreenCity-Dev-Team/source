﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZIPS.Classes;

namespace ZIPS.Forms
{
    public partial class frmCompany : ZIPS.frmBase
    {
        public frmCompany()
        {
            InitializeComponent();
        }

        private void frmCompany_Load(object sender, EventArgs e)
        {
            Suppress = true;
            Functions.FormatButton(btnSave);
            Functions.FormatButton(btnCancel);
            Functions.FormatButton(btnSelectLocation);

            IsReadOnly = (Properties.Settings.Default.Status == "Ready");

            PopulateData();

            if (IsReadOnly)
            {
                MakeReadOnly();
            }
            else
            {
                SetUpValidation();
            }
            Suppress = false;
            if (!IsReadOnly)
            {
                SetDirtyForBlankFields();
            }
        }

        private void MakeReadOnly()
        {
            btnBack.Visible = false;
            btnForward.Visible = false;
            btnSelectLocation.Visible = false;
            btnCancel.Visible = false;
            btnSave.Text = "Close";
            SetReadOnly(pnlTop);
            SetReadOnly(pnlStaticData);
        }

        private void SetDirtyForBlankFields()
        {
            if (String.IsNullOrEmpty(txtNextInvoiceNo.Text) || String.IsNullOrEmpty(txtTransaction.Text) || 
                String.IsNullOrEmpty(txtDeliveryTerms.Text) || String.IsNullOrEmpty(txtDueDate.Text) || 
                String.IsNullOrEmpty(txtAnticipatedDate.Text) || String.IsNullOrEmpty(txtDescription.Text) ||
                String.IsNullOrEmpty(txtVATCode.Text) || String.IsNullOrEmpty(txtAnalysisCode.Text) ||
                String.IsNullOrEmpty(txtDetailedNote.Text) || String.IsNullOrEmpty(txtBatchRef.Text) ||
                String.IsNullOrEmpty(txtCurrency.Text) || String.IsNullOrEmpty(lblCSVPath.Text) ||
                String.IsNullOrEmpty(txtCompanyAddress.Text) || String.IsNullOrEmpty(txtCompanyTelNos.Text) ||
                string.IsNullOrEmpty(txtVATRegNo.Text))
            {
                Dirty = true;
            }

        }

        private void SetUpValidation()
        {
            ValidationFields.Add(new clsFieldValidation(txtNextInvoiceNo, clsFieldValidation.InputTypes.Number, true));
            ValidationFields.Add(new clsFieldValidation(txtDefaultInvoiceText, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtDefaultTerms, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtCompanyTelNos, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtCompanyAddress, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtVATRegNo, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtTransaction, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtDeliveryTerms, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtDueDate, clsFieldValidation.InputTypes.Number, true));
            ValidationFields.Add(new clsFieldValidation(txtAnticipatedDate, clsFieldValidation.InputTypes.Number, true));
            ValidationFields.Add(new clsFieldValidation(txtDescription, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtVATCode, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtAnalysisCode, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtDetailedNote, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtBatchRef, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtCurrency, clsFieldValidation.InputTypes.Text, true));
            IdReqdFields();
            IdIndivReqdField(pnlCSVPath);
            FurtherValidation += new FurtherValidationEventHandler(FormSpecificValidation);
        }

        private void PopulateData()
        {
            // Populate data from settings where they are held
            lblInvoiceMonday.Text = Properties.Settings.Default.InvoiceMonday.ToShortDateString();
            txtDefaultInvoiceText.Text = Properties.Settings.Default.DefaultInvoiceText.ToString();
            txtDefaultTerms.Text = Properties.Settings.Default.DefaultTerms.ToString();
            txtCompanyAddress.Text = Properties.Settings.Default.CompanyAddress.ToString();
            txtCompanyTelNos.Text = Properties.Settings.Default.CompanyTelNos.ToString();
            txtVATRegNo.Text = Properties.Settings.Default.VATRegNo.ToString();
            txtNextInvoiceNo.Text = Properties.Settings.Default.NextInvoiceNo.ToString();
            txtTransaction.Text = Properties.Settings.Default.Transaction.ToString();
            txtDeliveryTerms.Text = Properties.Settings.Default.DeliveryTerms.ToString();
            txtDueDate.Text = Properties.Settings.Default.DueDate.ToString();
            txtAnticipatedDate.Text = Properties.Settings.Default.AnticipatedDate.ToString();
            txtDescription.Text = Properties.Settings.Default.Description.ToString();
            txtVATCode.Text = Properties.Settings.Default.VATCode.ToString();
            txtAnalysisCode.Text = Properties.Settings.Default.AnalysisCode.ToString();
            txtDetailedNote.Text = Properties.Settings.Default.DetailedNote.ToString();
            txtBatchRef.Text = Properties.Settings.Default.BatchRef.ToString();
            txtCurrency.Text = Properties.Settings.Default.InvoiceCurrency.ToString();
            lblCSVPath.Text = Properties.Settings.Default.CSVPath.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Dirty)
            {
                if (MessageBox.Show("You will lose any changes you have made. Do you want to continue?",Properties.Settings.Default.ShortSystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    Dirty = false;
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                }
            }
            else
            {
                Dirty = false;
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text == "Close")
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else if (IsValid)
            {
                int intNextInvoiceNo;
                if (!int.TryParse(txtNextInvoiceNo.Text, out intNextInvoiceNo))
                {
                    ep.SetError(txtNextInvoiceNo, "The next invoice number must be an integer");
                    return;
                }
                int intDueDate;
                if (!int.TryParse(txtDueDate.Text, out intDueDate))
                {
                    ep.SetError(txtDueDate, "The due date must be an integer");
                    return;
                }
                int intAnticipatedDate;
                if (!int.TryParse(txtAnticipatedDate.Text, out intAnticipatedDate))
                {
                    ep.SetError(txtAnticipatedDate, "The anticipated date must be an integer");
                    return;
                }
                if (string.IsNullOrEmpty(lblCSVPath.Text))
                {
                    ep.SetError(pnlCSVPath, "CSV Path must be selectd");
                    return;
                }
                Properties.Settings.Default.InvoiceMonday = DateTime.Parse(lblInvoiceMonday.Text);
                Properties.Settings.Default.DefaultInvoiceText = txtDefaultInvoiceText.Text;
                Properties.Settings.Default.DefaultTerms = txtDefaultTerms.Text;
                Properties.Settings.Default.CompanyAddress = txtCompanyAddress.Text;
                Properties.Settings.Default.CompanyTelNos = txtCompanyTelNos.Text;
                Properties.Settings.Default.VATRegNo = txtVATRegNo.Text;
                Properties.Settings.Default.NextInvoiceNo = intNextInvoiceNo;
                Properties.Settings.Default.Transaction = txtTransaction.Text;
                Properties.Settings.Default.DeliveryTerms = txtDeliveryTerms.Text;
                Properties.Settings.Default.DueDate= intDueDate;
                Properties.Settings.Default.AnticipatedDate = intAnticipatedDate;
                Properties.Settings.Default.Description = txtDescription.Text;
                Properties.Settings.Default.VATCode = txtVATCode.Text;
                Properties.Settings.Default.AnalysisCode = txtAnalysisCode.Text;
                Properties.Settings.Default.DetailedNote = txtDetailedNote.Text;
                Properties.Settings.Default.BatchRef = txtBatchRef.Text;
                Properties.Settings.Default.InvoiceCurrency = txtCurrency.Text;
                Properties.Settings.Default.CSVPath = lblCSVPath.Text;
                Properties.Settings.Default.Save();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private bool FormSpecificValidation()
        {
            return true;
        }

        private void btnSelectLocation_Click(object sender, EventArgs e)
        {
            dgCSV.RootFolder = Environment.SpecialFolder.MyComputer;
            if (!string.IsNullOrEmpty(lblCSVPath.Text))
            {
                dgCSV.SelectedPath = lblCSVPath.Text;
            }
            DialogResult result = dgCSV.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                lblCSVPath.Text = dgCSV.SelectedPath;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Parse(lblInvoiceMonday.Text);
            dt = dt.AddDays(-7);
            lblInvoiceMonday.Text = dt.ToShortDateString();
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Parse(lblInvoiceMonday.Text);
            dt = dt.AddDays(7);
            lblInvoiceMonday.Text = dt.ToShortDateString();
        }
    }
}
