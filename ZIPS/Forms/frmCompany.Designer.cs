﻿namespace ZIPS.Forms
{
    partial class frmCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblInvoiceMonday = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNextInvoiceNo = new System.Windows.Forms.TextBox();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.txtDefaultTerms = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDefaultInvoiceText = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.pnlStaticData = new System.Windows.Forms.Panel();
            this.btnSelectLocation = new System.Windows.Forms.Button();
            this.pnlCSVPath = new System.Windows.Forms.Panel();
            this.lblCSVPath = new System.Windows.Forms.Label();
            this.txtCurrency = new System.Windows.Forms.TextBox();
            this.txtBatchRef = new System.Windows.Forms.TextBox();
            this.txtDetailedNote = new System.Windows.Forms.TextBox();
            this.txtAnalysisCode = new System.Windows.Forms.TextBox();
            this.txtVATCode = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtAnticipatedDate = new System.Windows.Forms.TextBox();
            this.txtDueDate = new System.Windows.Forms.TextBox();
            this.txtDeliveryTerms = new System.Windows.Forms.TextBox();
            this.txtTransaction = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dgCSV = new System.Windows.Forms.FolderBrowserDialog();
            this.txtCompanyAddress = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCompanyTelNos = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtVATRegNo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.pnlTop.SuspendLayout();
            this.pnlStaticData.SuspendLayout();
            this.pnlCSVPath.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnCancel);
            this.pnlFooter.Controls.Add(this.btnSave);
            this.pnlFooter.Location = new System.Drawing.Point(0, 537);
            this.pnlFooter.Size = new System.Drawing.Size(815, 44);
            this.pnlFooter.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(199, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Next Invoice Monday";
            // 
            // lblInvoiceMonday
            // 
            this.lblInvoiceMonday.AutoSize = true;
            this.lblInvoiceMonday.Location = new System.Drawing.Point(375, 18);
            this.lblInvoiceMonday.Name = "lblInvoiceMonday";
            this.lblInvoiceMonday.Size = new System.Drawing.Size(74, 16);
            this.lblInvoiceMonday.TabIndex = 3;
            this.lblInvoiceMonday.Text = "04/04/2016";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(199, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Next Invoice Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(360, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "C";
            // 
            // txtNextInvoiceNo
            // 
            this.txtNextInvoiceNo.Location = new System.Drawing.Point(378, 52);
            this.txtNextInvoiceNo.Name = "txtNextInvoiceNo";
            this.txtNextInvoiceNo.Size = new System.Drawing.Size(71, 23);
            this.txtNextInvoiceNo.TabIndex = 0;
            // 
            // pnlTop
            // 
            this.pnlTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTop.Controls.Add(this.txtVATRegNo);
            this.pnlTop.Controls.Add(this.label22);
            this.pnlTop.Controls.Add(this.txtCompanyTelNos);
            this.pnlTop.Controls.Add(this.label21);
            this.pnlTop.Controls.Add(this.txtCompanyAddress);
            this.pnlTop.Controls.Add(this.label20);
            this.pnlTop.Controls.Add(this.txtDefaultTerms);
            this.pnlTop.Controls.Add(this.label19);
            this.pnlTop.Controls.Add(this.txtDefaultInvoiceText);
            this.pnlTop.Controls.Add(this.label18);
            this.pnlTop.Controls.Add(this.label1);
            this.pnlTop.Controls.Add(this.txtNextInvoiceNo);
            this.pnlTop.Controls.Add(this.lblInvoiceMonday);
            this.pnlTop.Controls.Add(this.label3);
            this.pnlTop.Controls.Add(this.btnBack);
            this.pnlTop.Controls.Add(this.label2);
            this.pnlTop.Controls.Add(this.btnForward);
            this.pnlTop.Location = new System.Drawing.Point(12, 10);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(796, 265);
            this.pnlTop.TabIndex = 0;
            // 
            // txtDefaultTerms
            // 
            this.txtDefaultTerms.Location = new System.Drawing.Point(378, 110);
            this.txtDefaultTerms.Multiline = true;
            this.txtDefaultTerms.Name = "txtDefaultTerms";
            this.txtDefaultTerms.Size = new System.Drawing.Size(311, 43);
            this.txtDefaultTerms.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(199, 113);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 16);
            this.label19.TabIndex = 24;
            this.label19.Text = "Default Terms";
            // 
            // txtDefaultInvoiceText
            // 
            this.txtDefaultInvoiceText.Location = new System.Drawing.Point(378, 81);
            this.txtDefaultInvoiceText.Name = "txtDefaultInvoiceText";
            this.txtDefaultInvoiceText.Size = new System.Drawing.Size(311, 23);
            this.txtDefaultInvoiceText.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(199, 84);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 16);
            this.label18.TabIndex = 22;
            this.label18.Text = "Default Invoice Text";
            // 
            // btnBack
            // 
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Image = global::ZIPS.Properties.Resources.left;
            this.btnBack.Location = new System.Drawing.Point(328, 9);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(40, 34);
            this.btnBack.TabIndex = 4;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnForward
            // 
            this.btnForward.FlatAppearance.BorderSize = 0;
            this.btnForward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForward.Image = global::ZIPS.Properties.Resources.right;
            this.btnForward.Location = new System.Drawing.Point(455, 9);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(40, 34);
            this.btnForward.TabIndex = 5;
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // pnlStaticData
            // 
            this.pnlStaticData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlStaticData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStaticData.Controls.Add(this.btnSelectLocation);
            this.pnlStaticData.Controls.Add(this.pnlCSVPath);
            this.pnlStaticData.Controls.Add(this.txtCurrency);
            this.pnlStaticData.Controls.Add(this.txtBatchRef);
            this.pnlStaticData.Controls.Add(this.txtDetailedNote);
            this.pnlStaticData.Controls.Add(this.txtAnalysisCode);
            this.pnlStaticData.Controls.Add(this.txtVATCode);
            this.pnlStaticData.Controls.Add(this.txtDescription);
            this.pnlStaticData.Controls.Add(this.txtAnticipatedDate);
            this.pnlStaticData.Controls.Add(this.txtDueDate);
            this.pnlStaticData.Controls.Add(this.txtDeliveryTerms);
            this.pnlStaticData.Controls.Add(this.txtTransaction);
            this.pnlStaticData.Controls.Add(this.label17);
            this.pnlStaticData.Controls.Add(this.label16);
            this.pnlStaticData.Controls.Add(this.label15);
            this.pnlStaticData.Controls.Add(this.label14);
            this.pnlStaticData.Controls.Add(this.label13);
            this.pnlStaticData.Controls.Add(this.label12);
            this.pnlStaticData.Controls.Add(this.label11);
            this.pnlStaticData.Controls.Add(this.label9);
            this.pnlStaticData.Controls.Add(this.label10);
            this.pnlStaticData.Controls.Add(this.label8);
            this.pnlStaticData.Controls.Add(this.label7);
            this.pnlStaticData.Controls.Add(this.label6);
            this.pnlStaticData.Controls.Add(this.label5);
            this.pnlStaticData.Controls.Add(this.label4);
            this.pnlStaticData.Location = new System.Drawing.Point(12, 274);
            this.pnlStaticData.Name = "pnlStaticData";
            this.pnlStaticData.Size = new System.Drawing.Size(796, 251);
            this.pnlStaticData.TabIndex = 1;
            // 
            // btnSelectLocation
            // 
            this.btnSelectLocation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSelectLocation.FlatAppearance.BorderSize = 0;
            this.btnSelectLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectLocation.Image = global::ZIPS.Properties.Resources.folder;
            this.btnSelectLocation.Location = new System.Drawing.Point(733, 207);
            this.btnSelectLocation.Name = "btnSelectLocation";
            this.btnSelectLocation.Size = new System.Drawing.Size(28, 23);
            this.btnSelectLocation.TabIndex = 10;
            this.btnSelectLocation.UseVisualStyleBackColor = true;
            this.btnSelectLocation.Click += new System.EventHandler(this.btnSelectLocation_Click);
            // 
            // pnlCSVPath
            // 
            this.pnlCSVPath.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlCSVPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCSVPath.Controls.Add(this.lblCSVPath);
            this.pnlCSVPath.Location = new System.Drawing.Point(120, 207);
            this.pnlCSVPath.Name = "pnlCSVPath";
            this.pnlCSVPath.Size = new System.Drawing.Size(591, 23);
            this.pnlCSVPath.TabIndex = 28;
            // 
            // lblCSVPath
            // 
            this.lblCSVPath.AutoSize = true;
            this.lblCSVPath.Location = new System.Drawing.Point(2, 2);
            this.lblCSVPath.Name = "lblCSVPath";
            this.lblCSVPath.Size = new System.Drawing.Size(49, 16);
            this.lblCSVPath.TabIndex = 27;
            this.lblCSVPath.Text = "label18";
            // 
            // txtCurrency
            // 
            this.txtCurrency.Location = new System.Drawing.Point(520, 178);
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.Size = new System.Drawing.Size(60, 23);
            this.txtCurrency.TabIndex = 9;
            // 
            // txtBatchRef
            // 
            this.txtBatchRef.Location = new System.Drawing.Point(520, 146);
            this.txtBatchRef.Name = "txtBatchRef";
            this.txtBatchRef.Size = new System.Drawing.Size(100, 23);
            this.txtBatchRef.TabIndex = 8;
            // 
            // txtDetailedNote
            // 
            this.txtDetailedNote.Location = new System.Drawing.Point(520, 114);
            this.txtDetailedNote.Name = "txtDetailedNote";
            this.txtDetailedNote.Size = new System.Drawing.Size(241, 23);
            this.txtDetailedNote.TabIndex = 7;
            // 
            // txtAnalysisCode
            // 
            this.txtAnalysisCode.Location = new System.Drawing.Point(520, 82);
            this.txtAnalysisCode.Name = "txtAnalysisCode";
            this.txtAnalysisCode.Size = new System.Drawing.Size(60, 23);
            this.txtAnalysisCode.TabIndex = 6;
            // 
            // txtVATCode
            // 
            this.txtVATCode.Location = new System.Drawing.Point(520, 50);
            this.txtVATCode.Name = "txtVATCode";
            this.txtVATCode.Size = new System.Drawing.Size(60, 23);
            this.txtVATCode.TabIndex = 5;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(120, 178);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(256, 23);
            this.txtDescription.TabIndex = 4;
            // 
            // txtAnticipatedDate
            // 
            this.txtAnticipatedDate.Location = new System.Drawing.Point(120, 146);
            this.txtAnticipatedDate.Name = "txtAnticipatedDate";
            this.txtAnticipatedDate.Size = new System.Drawing.Size(60, 23);
            this.txtAnticipatedDate.TabIndex = 3;
            // 
            // txtDueDate
            // 
            this.txtDueDate.Location = new System.Drawing.Point(120, 114);
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Size = new System.Drawing.Size(60, 23);
            this.txtDueDate.TabIndex = 2;
            // 
            // txtDeliveryTerms
            // 
            this.txtDeliveryTerms.Location = new System.Drawing.Point(120, 82);
            this.txtDeliveryTerms.Name = "txtDeliveryTerms";
            this.txtDeliveryTerms.Size = new System.Drawing.Size(100, 23);
            this.txtDeliveryTerms.TabIndex = 1;
            // 
            // txtTransaction
            // 
            this.txtTransaction.Location = new System.Drawing.Point(120, 50);
            this.txtTransaction.Name = "txtTransaction";
            this.txtTransaction.Size = new System.Drawing.Size(100, 23);
            this.txtTransaction.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 210);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 16);
            this.label17.TabIndex = 16;
            this.label17.Text = "CSV Path";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(413, 181);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 16);
            this.label16.TabIndex = 15;
            this.label16.Text = "Currency";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(413, 149);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 16);
            this.label15.TabIndex = 14;
            this.label15.Text = "Batch Ref";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(413, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 16);
            this.label14.TabIndex = 13;
            this.label14.Text = "Detailed Note";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(413, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 16);
            this.label13.TabIndex = 12;
            this.label13.Text = "Analysis Code";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(413, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 16);
            this.label12.TabIndex = 11;
            this.label12.Text = "VAT  Code";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 181);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 16);
            this.label11.TabIndex = 10;
            this.label11.Text = "Description";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(205, 149);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 16);
            this.label9.TabIndex = 9;
            this.label9.Text = "days after due date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 149);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 16);
            this.label10.TabIndex = 8;
            this.label10.Text = "Anticipated Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "days after invoice date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Due Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Delivery Terms";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Transaction";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(284, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Export File Static Data";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(728, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 32);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.CausesValidation = false;
            this.btnCancel.Location = new System.Drawing.Point(370, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtCompanyAddress
            // 
            this.txtCompanyAddress.Location = new System.Drawing.Point(378, 159);
            this.txtCompanyAddress.Name = "txtCompanyAddress";
            this.txtCompanyAddress.Size = new System.Drawing.Size(311, 23);
            this.txtCompanyAddress.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(199, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 16);
            this.label20.TabIndex = 26;
            this.label20.Text = "Company Address";
            // 
            // txtCompanyTelNos
            // 
            this.txtCompanyTelNos.Location = new System.Drawing.Point(378, 188);
            this.txtCompanyTelNos.Name = "txtCompanyTelNos";
            this.txtCompanyTelNos.Size = new System.Drawing.Size(311, 23);
            this.txtCompanyTelNos.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(199, 191);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(150, 16);
            this.label21.TabIndex = 28;
            this.label21.Text = "Company Telephone Nos";
            // 
            // txtVATRegNo
            // 
            this.txtVATRegNo.Location = new System.Drawing.Point(378, 217);
            this.txtVATRegNo.Name = "txtVATRegNo";
            this.txtVATRegNo.Size = new System.Drawing.Size(311, 23);
            this.txtVATRegNo.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(199, 220);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 16);
            this.label22.TabIndex = 30;
            this.label22.Text = "VAT Reg No";
            // 
            // frmCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(815, 581);
            this.ControlBox = false;
            this.Controls.Add(this.pnlStaticData);
            this.Controls.Add(this.pnlTop);
            this.DoubleBuffered = true;
            this.Name = "frmCompany";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Company Data";
            this.Load += new System.EventHandler(this.frmCompany_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.pnlTop, 0);
            this.Controls.SetChildIndex(this.pnlStaticData, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlStaticData.ResumeLayout(false);
            this.pnlStaticData.PerformLayout();
            this.pnlCSVPath.ResumeLayout(false);
            this.pnlCSVPath.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Label lblInvoiceMonday;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNextInvoiceNo;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlStaticData;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTransaction;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtCurrency;
        private System.Windows.Forms.TextBox txtBatchRef;
        private System.Windows.Forms.TextBox txtDetailedNote;
        private System.Windows.Forms.TextBox txtAnalysisCode;
        private System.Windows.Forms.TextBox txtVATCode;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtAnticipatedDate;
        private System.Windows.Forms.TextBox txtDueDate;
        private System.Windows.Forms.TextBox txtDeliveryTerms;
        private System.Windows.Forms.Button btnSelectLocation;
        private System.Windows.Forms.Panel pnlCSVPath;
        private System.Windows.Forms.Label lblCSVPath;
        private System.Windows.Forms.FolderBrowserDialog dgCSV;
        private System.Windows.Forms.TextBox txtDefaultTerms;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDefaultInvoiceText;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtVATRegNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCompanyTelNos;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtCompanyAddress;
        private System.Windows.Forms.Label label20;
    }
}
