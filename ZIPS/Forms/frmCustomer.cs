﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZIPS.Classes;
using ZIPS.Data;

namespace ZIPS.Forms
{
    public partial class frmCustomer : ZIPS.frmBase
    {
        public frmCustomer()
        {
            InitializeComponent();
        }

        public Data.Customer thisCustomer { get; set; }

        private void frmCustomer_Load(object sender, EventArgs e)
        {
            Suppress = true;
            Functions.FormatButton(btnCancel);
            Functions.FormatButton(btnSave);
            LoadData();
            IsReadOnly = (Properties.Settings.Default.Status == "Ready");
            if (IsReadOnly)
            {
                MakeReadOnly();
            }
            else
            {
                SetUpValidation();
            }

            Suppress = false;
            //if we're adding a new customer then set the form to dirty to force validation on save
            if (thisCustomer.Id == 0)
                Dirty = true;
        }

        private void SetUpValidation()
        {
            ValidationFields.Add(new clsFieldValidation(txtCode, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtName, clsFieldValidation.InputTypes.Text, true));
            IdReqdFields();
            FurtherValidation += new FurtherValidationEventHandler(FormSpecificValidation);
        }

        private bool FormSpecificValidation()
        {
            return true;
        }

        private void MakeReadOnly()
        {
            btnCancel.Visible = false;
            btnSave.Text = "Close";
            SetReadOnly(this);
        }

        private void LoadData()
        {
            txtCode.Text = thisCustomer.AccountNumber ?? string.Empty;
            txtName.Text = thisCustomer.Name ?? string.Empty;
            txtAddress1.Text = thisCustomer.Address1 ?? string.Empty;
            txtAddress2.Text = thisCustomer.Address2 ?? string.Empty;
            txtTown.Text = thisCustomer.Town ?? string.Empty;
            txtPostCode.Text = thisCustomer.PostCode ?? string.Empty;
            if (string.IsNullOrEmpty(thisCustomer.InvoiceText))
                txtInvoiceText.Text = Properties.Settings.Default.DefaultInvoiceText;
            else
                txtInvoiceText.Text = thisCustomer.InvoiceText;
            if (string.IsNullOrEmpty(thisCustomer.Terms))
                txtTerms.Text = Properties.Settings.Default.DefaultTerms;
            else
                txtTerms.Text = thisCustomer.Terms;

            switch (thisCustomer.InvoiceOffset)
            {
                case 0:
                    rbMonday.Checked = true;
                    break;
                case 1:
                    rbTuesday.Checked = true;
                    break;
                case 2:
                    rbWednesday.Checked = true;
                    break;
                case 3:
                    rbThursday.Checked = true;
                    break;
                case 4:
                    rbFriday.Checked = true;
                    break;
                case 5:
                    rbSaturday.Checked = true;
                    break;
                case 6:
                    rbSunday.Checked = true;
                    break;
                default:
                    rbMonday.Checked = true;
                    break;
            }
                
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Dirty)
            {
                if (MessageBox.Show("You will lose any changes you have made. Do you want to continue?", Properties.Settings.Default.ShortSystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    Dirty = false;
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                }
            }
            else
            {
                Dirty = false;
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text == "Close")
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else if (IsValid)
            {
                thisCustomer.AccountNumber = txtCode.Text;
                thisCustomer.Name = txtName.Text;
                thisCustomer.Address1 = txtAddress1.Text;
                thisCustomer.Address2 = txtAddress2.Text;
                thisCustomer.Town = txtTown.Text;
                thisCustomer.PostCode = txtPostCode.Text;
                thisCustomer.InvoiceText = txtInvoiceText.Text;
                thisCustomer.Terms = txtTerms.Text;
                if (rbMonday.Checked)
                    thisCustomer.InvoiceOffset = 0;
                else if (rbTuesday.Checked)
                    thisCustomer.InvoiceOffset = 1;
                else if (rbWednesday.Checked)
                    thisCustomer.InvoiceOffset = 2;
                else if (rbThursday.Checked)
                    thisCustomer.InvoiceOffset = 3;
                else if (rbFriday.Checked)
                    thisCustomer.InvoiceOffset = 4;
                else if (rbSaturday.Checked)
                    thisCustomer.InvoiceOffset = 5;
                else if (rbSunday.Checked)
                    thisCustomer.InvoiceOffset = 6;

                if (thisCustomer.Id == 0)
                {
                    if (CustomerDAL.AddCustomer(thisCustomer))
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    if (CustomerDAL.UpdateCustomer(thisCustomer))
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                
            }
        }
    }
}
