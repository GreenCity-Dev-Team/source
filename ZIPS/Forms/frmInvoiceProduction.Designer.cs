﻿namespace ZIPS.Forms
{
    partial class frmInvoiceProduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInvoiceProduction));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnImportBalances = new System.Windows.Forms.Button();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMondayDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblPeriod = new System.Windows.Forms.Label();
            this.nudMonth = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbNext = new System.Windows.Forms.RadioButton();
            this.rbCurrent = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnNextWeek = new System.Windows.Forms.Button();
            this.btnCSV = new System.Windows.Forms.Button();
            this.btnPrintDriverCustomer = new System.Windows.Forms.Button();
            this.btnPrintOffice = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbReady = new System.Windows.Forms.RadioButton();
            this.rbPrepare = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.grpSort = new System.Windows.Forms.GroupBox();
            this.rbDateAndAccount = new System.Windows.Forms.RadioButton();
            this.rbDate = new System.Windows.Forms.RadioButton();
            this.dgvInvoices = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OutstandingBalanceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceAmtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceNoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceThisWeekColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnClose = new System.Windows.Forms.Button();
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remoteSupportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dlgBackup = new System.Windows.Forms.SaveFileDialog();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnSelectNone = new System.Windows.Forms.Button();
            this.lblVersionText = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonth)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.grpSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoices)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.lblVersionText);
            this.pnlFooter.Controls.Add(this.btnSelectNone);
            this.pnlFooter.Controls.Add(this.btnSelectAll);
            this.pnlFooter.Location = new System.Drawing.Point(0, 662);
            this.pnlFooter.Size = new System.Drawing.Size(1001, 44);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnImportBalances);
            this.panel1.Controls.Add(this.lblInvoiceNo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblMondayDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(252, 142);
            this.panel1.TabIndex = 2;
            // 
            // btnImportBalances
            // 
            this.btnImportBalances.Location = new System.Drawing.Point(46, 107);
            this.btnImportBalances.Name = "btnImportBalances";
            this.btnImportBalances.Size = new System.Drawing.Size(138, 28);
            this.btnImportBalances.TabIndex = 4;
            this.btnImportBalances.Text = "Import Balances";
            this.btnImportBalances.UseVisualStyleBackColor = true;
            this.btnImportBalances.Click += new System.EventHandler(this.btnImportBalances_Click);
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.Location = new System.Drawing.Point(163, 76);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(43, 16);
            this.lblInvoiceNo.TabIndex = 3;
            this.lblInvoiceNo.Text = "50001";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "First Invoice Number:";
            // 
            // lblMondayDate
            // 
            this.lblMondayDate.AutoSize = true;
            this.lblMondayDate.Location = new System.Drawing.Point(22, 39);
            this.lblMondayDate.Name = "lblMondayDate";
            this.lblMondayDate.Size = new System.Drawing.Size(171, 16);
            this.lblMondayDate.TabIndex = 1;
            this.lblMondayDate.Text = "Monday 22nd February 2016";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Invoices for Week Commencing";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblPeriod);
            this.panel2.Controls.Add(this.nudMonth);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(258, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(164, 149);
            this.panel2.TabIndex = 3;
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(6, 57);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(44, 16);
            this.lblPeriod.TabIndex = 2;
            this.lblPeriod.Text = "Period";
            // 
            // nudMonth
            // 
            this.nudMonth.Location = new System.Drawing.Point(56, 55);
            this.nudMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMonth.Name = "nudMonth";
            this.nudMonth.Size = new System.Drawing.Size(45, 23);
            this.nudMonth.TabIndex = 1;
            this.nudMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMonth.ValueChanged += new System.EventHandler(this.nudMonth_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbNext);
            this.groupBox2.Controls.Add(this.rbCurrent);
            this.groupBox2.Location = new System.Drawing.Point(4, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(153, 49);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Year";
            // 
            // rbNext
            // 
            this.rbNext.AutoSize = true;
            this.rbNext.Location = new System.Drawing.Point(83, 23);
            this.rbNext.Name = "rbNext";
            this.rbNext.Size = new System.Drawing.Size(51, 20);
            this.rbNext.TabIndex = 1;
            this.rbNext.TabStop = true;
            this.rbNext.Text = "Next";
            this.rbNext.UseVisualStyleBackColor = true;
            this.rbNext.CheckedChanged += new System.EventHandler(this.rbCurrentNextChanged);
            // 
            // rbCurrent
            // 
            this.rbCurrent.AutoSize = true;
            this.rbCurrent.Location = new System.Drawing.Point(7, 23);
            this.rbCurrent.Name = "rbCurrent";
            this.rbCurrent.Size = new System.Drawing.Size(69, 20);
            this.rbCurrent.TabIndex = 0;
            this.rbCurrent.TabStop = true;
            this.rbCurrent.Text = "Current";
            this.rbCurrent.UseVisualStyleBackColor = true;
            this.rbCurrent.CheckedChanged += new System.EventHandler(this.rbCurrentNextChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnNextWeek);
            this.panel3.Controls.Add(this.btnCSV);
            this.panel3.Controls.Add(this.btnPrintDriverCustomer);
            this.panel3.Controls.Add(this.btnPrintOffice);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Location = new System.Drawing.Point(442, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(299, 142);
            this.panel3.TabIndex = 4;
            // 
            // btnNextWeek
            // 
            this.btnNextWeek.Location = new System.Drawing.Point(151, 107);
            this.btnNextWeek.Name = "btnNextWeek";
            this.btnNextWeek.Size = new System.Drawing.Size(138, 28);
            this.btnNextWeek.TabIndex = 4;
            this.btnNextWeek.Text = "Next Week";
            this.btnNextWeek.UseVisualStyleBackColor = true;
            this.btnNextWeek.Click += new System.EventHandler(this.btnNextWeek_Click);
            // 
            // btnCSV
            // 
            this.btnCSV.Location = new System.Drawing.Point(6, 107);
            this.btnCSV.Name = "btnCSV";
            this.btnCSV.Size = new System.Drawing.Size(138, 28);
            this.btnCSV.TabIndex = 3;
            this.btnCSV.Text = "Create CSV";
            this.btnCSV.UseVisualStyleBackColor = true;
            this.btnCSV.Click += new System.EventHandler(this.btnCSV_Click);
            // 
            // btnPrintDriverCustomer
            // 
            this.btnPrintDriverCustomer.Location = new System.Drawing.Point(6, 76);
            this.btnPrintDriverCustomer.Name = "btnPrintDriverCustomer";
            this.btnPrintDriverCustomer.Size = new System.Drawing.Size(283, 28);
            this.btnPrintDriverCustomer.TabIndex = 2;
            this.btnPrintDriverCustomer.Text = "Print Driver && Customer Copies";
            this.btnPrintDriverCustomer.UseVisualStyleBackColor = true;
            this.btnPrintDriverCustomer.Click += new System.EventHandler(this.btnPrintDriverCustomer_Click);
            // 
            // btnPrintOffice
            // 
            this.btnPrintOffice.Location = new System.Drawing.Point(6, 45);
            this.btnPrintOffice.Name = "btnPrintOffice";
            this.btnPrintOffice.Size = new System.Drawing.Size(283, 28);
            this.btnPrintOffice.TabIndex = 1;
            this.btnPrintOffice.Text = "Print Office Copies";
            this.btnPrintOffice.UseVisualStyleBackColor = true;
            this.btnPrintOffice.Click += new System.EventHandler(this.btnPrintOffice_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbReady);
            this.groupBox1.Controls.Add(this.rbPrepare);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 43);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // rbReady
            // 
            this.rbReady.AutoSize = true;
            this.rbReady.Location = new System.Drawing.Point(179, 14);
            this.rbReady.Name = "rbReady";
            this.rbReady.Size = new System.Drawing.Size(61, 20);
            this.rbReady.TabIndex = 1;
            this.rbReady.TabStop = true;
            this.rbReady.Text = "Ready";
            this.rbReady.UseVisualStyleBackColor = true;
            this.rbReady.CheckedChanged += new System.EventHandler(this.rbPrepareReadyChanged);
            // 
            // rbPrepare
            // 
            this.rbPrepare.AutoSize = true;
            this.rbPrepare.Location = new System.Drawing.Point(39, 14);
            this.rbPrepare.Name = "rbPrepare";
            this.rbPrepare.Size = new System.Drawing.Size(71, 20);
            this.rbPrepare.TabIndex = 0;
            this.rbPrepare.TabStop = true;
            this.rbPrepare.Text = "Prepare";
            this.rbPrepare.UseVisualStyleBackColor = true;
            this.rbPrepare.CheckedChanged += new System.EventHandler(this.rbPrepareReadyChanged);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.grpSort);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.panel1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 24);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1001, 149);
            this.panel4.TabIndex = 5;
            // 
            // grpSort
            // 
            this.grpSort.Controls.Add(this.rbDateAndAccount);
            this.grpSort.Controls.Add(this.rbDate);
            this.grpSort.Location = new System.Drawing.Point(747, 83);
            this.grpSort.Name = "grpSort";
            this.grpSort.Size = new System.Drawing.Size(242, 52);
            this.grpSort.TabIndex = 6;
            this.grpSort.TabStop = false;
            this.grpSort.Text = "Sort By";
            this.grpSort.Visible = false;
            // 
            // rbDateAndAccount
            // 
            this.rbDateAndAccount.AutoSize = true;
            this.rbDateAndAccount.Checked = true;
            this.rbDateAndAccount.Location = new System.Drawing.Point(101, 23);
            this.rbDateAndAccount.Name = "rbDateAndAccount";
            this.rbDateAndAccount.Size = new System.Drawing.Size(114, 20);
            this.rbDateAndAccount.TabIndex = 1;
            this.rbDateAndAccount.TabStop = true;
            this.rbDateAndAccount.Text = "Date && Account";
            this.rbDateAndAccount.UseVisualStyleBackColor = true;
            this.rbDateAndAccount.CheckedChanged += new System.EventHandler(this.rbInvoiceSortChanged);
            // 
            // rbDate
            // 
            this.rbDate.AutoSize = true;
            this.rbDate.Location = new System.Drawing.Point(17, 23);
            this.rbDate.Name = "rbDate";
            this.rbDate.Size = new System.Drawing.Size(52, 20);
            this.rbDate.TabIndex = 0;
            this.rbDate.Text = "Date";
            this.rbDate.UseVisualStyleBackColor = true;
            this.rbDate.CheckedChanged += new System.EventHandler(this.rbInvoiceSortChanged);
            // 
            // dgvInvoices
            // 
            this.dgvInvoices.AllowUserToAddRows = false;
            this.dgvInvoices.AllowUserToDeleteRows = false;
            this.dgvInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.AccountNumberColumn,
            this.NameColumn,
            this.OutstandingBalanceColumn,
            this.InvoiceAmtColumn,
            this.InvoiceNoColumn,
            this.InvoiceDateColumn,
            this.InvoiceThisWeekColumn});
            this.dgvInvoices.Location = new System.Drawing.Point(0, 173);
            this.dgvInvoices.Name = "dgvInvoices";
            this.dgvInvoices.RowHeadersVisible = false;
            this.dgvInvoices.Size = new System.Drawing.Size(1001, 492);
            this.dgvInvoices.TabIndex = 6;
            this.dgvInvoices.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoices_RowValidated);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.Visible = false;
            // 
            // AccountNumberColumn
            // 
            this.AccountNumberColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AccountNumberColumn.DataPropertyName = "AccountNumber";
            this.AccountNumberColumn.HeaderText = "Customer Code";
            this.AccountNumberColumn.Name = "AccountNumberColumn";
            this.AccountNumberColumn.Width = 109;
            // 
            // NameColumn
            // 
            this.NameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameColumn.DataPropertyName = "Name";
            this.NameColumn.HeaderText = "Customer Name";
            this.NameColumn.Name = "NameColumn";
            // 
            // OutstandingBalanceColumn
            // 
            this.OutstandingBalanceColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.OutstandingBalanceColumn.DataPropertyName = "OutstandingBalance";
            this.OutstandingBalanceColumn.HeaderText = "Balance Forward";
            this.OutstandingBalanceColumn.Name = "OutstandingBalanceColumn";
            this.OutstandingBalanceColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OutstandingBalanceColumn.Width = 97;
            // 
            // InvoiceAmtColumn
            // 
            this.InvoiceAmtColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.InvoiceAmtColumn.DataPropertyName = "InvoiceAmt";
            this.InvoiceAmtColumn.HeaderText = "Invoice Net";
            this.InvoiceAmtColumn.Name = "InvoiceAmtColumn";
            this.InvoiceAmtColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.InvoiceAmtColumn.Width = 67;
            // 
            // InvoiceNoColumn
            // 
            this.InvoiceNoColumn.DataPropertyName = "InvoiceNo";
            this.InvoiceNoColumn.HeaderText = "Invoice No";
            this.InvoiceNoColumn.Name = "InvoiceNoColumn";
            this.InvoiceNoColumn.ReadOnly = true;
            this.InvoiceNoColumn.Visible = false;
            // 
            // InvoiceDateColumn
            // 
            this.InvoiceDateColumn.DataPropertyName = "InvoiceDate";
            this.InvoiceDateColumn.HeaderText = "Invoice Date";
            this.InvoiceDateColumn.Name = "InvoiceDateColumn";
            this.InvoiceDateColumn.ReadOnly = true;
            this.InvoiceDateColumn.Visible = false;
            // 
            // InvoiceThisWeekColumn
            // 
            this.InvoiceThisWeekColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.InvoiceThisWeekColumn.DataPropertyName = "InvoiceThisWeek";
            this.InvoiceThisWeekColumn.HeaderText = "Include";
            this.InvoiceThisWeekColumn.Name = "InvoiceThisWeekColumn";
            this.InvoiceThisWeekColumn.Width = 53;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(746, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dlgOpenFile
            // 
            this.dlgOpenFile.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.companyDataToolStripMenuItem,
            this.customersToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1001, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem,
            this.restoreToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.backupToolStripMenuItem.Text = "Backup";
            this.backupToolStripMenuItem.Click += new System.EventHandler(this.backupToolStripMenuItem_Click);
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            this.restoreToolStripMenuItem.Click += new System.EventHandler(this.restoreToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // companyDataToolStripMenuItem
            // 
            this.companyDataToolStripMenuItem.Name = "companyDataToolStripMenuItem";
            this.companyDataToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.companyDataToolStripMenuItem.Text = "Company Data";
            this.companyDataToolStripMenuItem.Click += new System.EventHandler(this.companyDataToolStripMenuItem_Click);
            // 
            // customersToolStripMenuItem
            // 
            this.customersToolStripMenuItem.Name = "customersToolStripMenuItem";
            this.customersToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.customersToolStripMenuItem.Text = "Customers";
            this.customersToolStripMenuItem.Click += new System.EventHandler(this.customersToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.remoteSupportToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // remoteSupportToolStripMenuItem
            // 
            this.remoteSupportToolStripMenuItem.Name = "remoteSupportToolStripMenuItem";
            this.remoteSupportToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.remoteSupportToolStripMenuItem.Text = "Remote Support";
            this.remoteSupportToolStripMenuItem.Click += new System.EventHandler(this.remoteSupportToolStripMenuItem_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(885, 9);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(94, 28);
            this.btnSelectAll.TabIndex = 0;
            this.btnSelectAll.Text = "Select All";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnSelectNone
            // 
            this.btnSelectNone.Location = new System.Drawing.Point(776, 9);
            this.btnSelectNone.Name = "btnSelectNone";
            this.btnSelectNone.Size = new System.Drawing.Size(94, 28);
            this.btnSelectNone.TabIndex = 1;
            this.btnSelectNone.Text = "Select None";
            this.btnSelectNone.UseVisualStyleBackColor = true;
            this.btnSelectNone.Click += new System.EventHandler(this.btnSelectNone_Click);
            // 
            // lblVersionText
            // 
            this.lblVersionText.AutoSize = true;
            this.lblVersionText.Location = new System.Drawing.Point(12, 15);
            this.lblVersionText.Name = "lblVersionText";
            this.lblVersionText.Size = new System.Drawing.Size(42, 16);
            this.lblVersionText.TabIndex = 2;
            this.lblVersionText.Text = "label3";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::ZIPS.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(822, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(167, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // frmInvoiceProduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(1001, 706);
            this.Controls.Add(this.dgvInvoices);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmInvoiceProduction";
            this.ShowInTaskbar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ZIP Invoice Production System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmInvoiceProduction_FormClosing);
            this.Load += new System.EventHandler(this.frmInvoiceProduction_Load);
            this.Controls.SetChildIndex(this.menuStrip1, 0);
            this.Controls.SetChildIndex(this.panel4, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.dgvInvoices, 0);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.pnlFooter.ResumeLayout(false);
            this.pnlFooter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonth)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.grpSort.ResumeLayout(false);
            this.grpSort.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoices)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnImportBalances;
        private System.Windows.Forms.Label lblInvoiceNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblMondayDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblPeriod;
        private System.Windows.Forms.NumericUpDown nudMonth;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbNext;
        private System.Windows.Forms.RadioButton rbCurrent;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnNextWeek;
        private System.Windows.Forms.Button btnCSV;
        private System.Windows.Forms.Button btnPrintDriverCustomer;
        private System.Windows.Forms.Button btnPrintOffice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbReady;
        private System.Windows.Forms.RadioButton rbPrepare;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgvInvoices;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn OutstandingBalanceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceAmtColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceNoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceDateColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn InvoiceThisWeekColumn;
        private System.Windows.Forms.OpenFileDialog dlgOpenFile;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog dlgBackup;
        private System.Windows.Forms.Button btnSelectNone;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.GroupBox grpSort;
        private System.Windows.Forms.RadioButton rbDateAndAccount;
        private System.Windows.Forms.RadioButton rbDate;
        private System.Windows.Forms.Label lblVersionText;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remoteSupportToolStripMenuItem;
    }
}
