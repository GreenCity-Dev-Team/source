﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.IO;
using ZIPS.Classes;
using ZIPS.Data;

namespace ZIPS.Forms
{
    public partial class frmCustomerList : ZIPS.frmBase
    {
        private SortableBindingList<Data.Customer> CustomerList;

        public frmCustomerList()
        {
            InitializeComponent();
        }

        private void frmCustomerList_Load(object sender, EventArgs e)
        {
            Suppress = true;
            loading = true;
            Functions.StyleDataGridView(dgvCustomers);
            Functions.FormatButton(btnAddCustomer);
            Functions.FormatButton(btnClose);
            //Disable add customer button if status is "Ready"
            if (Properties.Settings.Default.Status == "Ready")
            {
                btnAddCustomer.Enabled = false;
                dgvCustomers.Columns["DeleteButton"].Visible = false;
            }
            LoadGrid();
        }

        private void LoadGrid(int selectedId = 0)
        {
            gridLoading = true;
            int intCurrTopRow = dgvCustomers.FirstDisplayedScrollingRowIndex;
            if (intCurrTopRow < 0)
            {
                intCurrTopRow = 0;
            }

            CustomerList = CustomerDAL.GetCustomerList();
            dgvCustomers.DataSource = CustomerList;

            int intRowOfSelItem = 0;
            if (selectedId != 0)
            {
                dgvCustomers.FirstDisplayedScrollingRowIndex = intCurrTopRow;
                foreach (DataGridViewRow r in dgvCustomers.Rows)
                {
                    if ((int)r.Cells["CustomerId"].Value == selectedId)
                    {
                        intRowOfSelItem = r.Index;
                    }
                }
                dgvCustomers.CurrentCell = dgvCustomers.Rows[intRowOfSelItem].Cells["CustomerName"];
                dgvCustomers.Rows[intRowOfSelItem].Selected = true;
            }

            if (selectedId == 0)
            {
                dgvCustomers.Sort(dgvCustomers.Columns["CustomerName"], ListSortDirection.Ascending);
            }

            gridLoading = false;
        }



        private void dgvCustomers_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvCustomers.Columns["DeleteButton"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.Left + 4, e.CellBounds.Top + 2, 16, 16);
                    e.Handled = true;
                }
            }
        }

        private void dgvCustomers_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            if (e.RowIndex >= 0  && e.ColumnIndex >= 0)
            {
                e.ToolTipText = dgvCustomers.Columns[e.ColumnIndex].ToolTipText;
            }
        }

        private void dgvCustomers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvCustomers.Columns["DeleteButton"].Index)
                {
                    if (MessageBox.Show(string.Format("Are you sure you want to delete customer {0}?", 
                                            dgvCustomers.Rows[e.RowIndex].Cells["CustomerName"].Value), 
                                            Properties.Settings.Default.ShortSystemName,
                                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        CustomerDAL.DeleteCustomer(int.Parse(dgvCustomers.Rows[e.RowIndex].Cells["CustomerId"].Value.ToString()));
                        int refreshId;
                        //set code to refresh to to be code of next row unless we are deleting last row, in that case set to prev row
                        if (dgvCustomers.RowCount == e.RowIndex + 1)
                        {
                            if (e.RowIndex == 0)
                            {
                                refreshId = 0;
                            }
                            else
                            {
                                refreshId = (int)dgvCustomers.Rows[e.RowIndex - 1].Cells["CustomerId"].Value;
                            }
                        }
                        else
                        {
                            refreshId = (int)dgvCustomers.Rows[e.RowIndex + 1].Cells["CustomerId"].Value;
                        }
                        LoadGrid(refreshId);
                    }
                }
            }
        }

        private void dgvCustomers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex >= 0 && e.ColumnIndex != dgvCustomers.Columns["DeleteButton"].Index)
                {
                    int customerId = int.Parse(dgvCustomers.Rows[e.RowIndex].Cells["CustomerId"].Value.ToString());
                    frmCustomer frm = new frmCustomer();
                    frm.thisCustomer = CustomerDAL.GetCustomer(customerId);
                    frm.ShowDialog();
                }
            }
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            frmCustomer frm = new frmCustomer();
            frm.thisCustomer = new Customer();
            frm.ShowDialog();
            LoadGrid();
        }
    }
}
