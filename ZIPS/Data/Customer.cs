﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZIPS.Data
{
    public class Customer
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
        public int InvoiceOffset { get; set; }
        public string InvoiceText { get; set; }
        public decimal OutstandingBalance { get; set; }
        public decimal InvoiceAmt { get; set; }
        public bool InvoiceThisWeek { get; set; }
        public string Terms { get; set; }
        public int InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
    }
}
