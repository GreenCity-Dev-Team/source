﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;
using ZIPS.Classes;

namespace ZIPS.Data
{
    public static class CustomerDAL
    {
        public static SortableBindingList<Data.Customer> GetCustomerList()
        {
            try
            {
                SortableBindingList<Data.Customer> CustomerList = new SortableBindingList<Customer>();

                if (!File.Exists(Globals.dataFilePath))
                {
                    Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                    Exceptions.HandleException(ex, "CustomerDAL_GetCustomerList", true);
                    return null;
                }
                else
                {
                    XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                    IEnumerable<XElement> customers = xmlDoc.Descendants("customer");

                    Data.Customer customer;
                    int maxCustomerId = 0;
                    foreach (var c in customers)
                    {
                        customer = PopulateCustomerInstance(c);
                        if (customer.Id > maxCustomerId)
                        {
                            maxCustomerId = customer.Id;
                        }
                        CustomerList.Add(customer);
                    }
                    Properties.Settings.Default.MaxCustomerId = maxCustomerId;
                    Properties.Settings.Default.Save();
                    return CustomerList;
                }
            }
            catch (Exception ex1)
            {
                Exceptions.HandleException(ex1, "CustomerDAL_GetCustomerList", true);
                return null;
            }

        }

        public static List<Data.Customer> GetSortedCustomerList(bool sortByInvDateAndAccNo)
        {
            List<Customer> customerList = new List<Customer>();
            if (!File.Exists(Globals.dataFilePath))
            {
                Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                Exceptions.HandleException(ex, "CustomerDAL_GetCustomerList", true);
                return null;
            }
            else
            {
                XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                IEnumerable<XElement> customers = xmlDoc.Descendants("customer");

                Data.Customer customer;
                foreach (var c in customers)
                {
                    customer = PopulateCustomerInstance(c);
                    if (customer.InvoiceThisWeek)
                    {
                        customerList.Add(customer);
                    }
                }
                if (sortByInvDateAndAccNo)
                {
                    //Sort by invoice date then account number
                    return customerList.OrderBy(c => c.InvoiceDate).ThenBy(c => c.AccountNumber).ToList();
                }
                else
                {
                    //Just sort by invoice date
                    return customerList.OrderBy(c => c.InvoiceDate).ToList();
                }
            }
        }

        private static Customer PopulateCustomerInstance(XElement x)
        {
            string Id;
            int intId;
            string InvoiceOffset;
            int intInvoiceOffset;
            string OutstandingBalance;
            decimal decOutstandingBalance;
            string InvoiceAmt;
            decimal decInvoiceAmt;
            string InvoiceThisWeek;
            bool blnInvoiceThisWeek;
            string InvoiceNo;
            int intInvoiceNo;
            string InvoiceDate;
            DateTime dtInvoiceDate;
            Customer c;

            c = new Customer();
            Id = x.TryGetElementValue("Id", "0");
            if (int.TryParse(Id, out intId))
                c.Id = intId;
            else
                c.Id = 0;
            c.AccountNumber = x.TryGetElementValue("AccountNumber", "");
            c.Name = x.TryGetElementValue("Name", "");
            c.Address1 = x.TryGetElementValue("Address1", "");
            c.Address2 = x.TryGetElementValue("Address2", "");
            c.Town = x.TryGetElementValue("Town", "");
            c.PostCode = x.TryGetElementValue("PostCode", "");
            InvoiceOffset = x.TryGetElementValue("InvoiceOffset", "0");
            if (int.TryParse(InvoiceOffset, out intInvoiceOffset))
            {
                c.InvoiceOffset = intInvoiceOffset;
            }
            else
            {
                c.InvoiceOffset = 0;
            }
            c.InvoiceText = x.TryGetElementValue("InvoiceText", "");
            OutstandingBalance = x.TryGetElementValue("OutstandingBalance", "0");
            if (decimal.TryParse(OutstandingBalance, out decOutstandingBalance))
            {
                c.OutstandingBalance = decOutstandingBalance;
            }
            else
            {
                c.OutstandingBalance = 0;
            }
            InvoiceAmt = x.TryGetElementValue("InvoiceAmt", "0");
            if (decimal.TryParse(InvoiceAmt, out decInvoiceAmt))
            {
                c.InvoiceAmt = decInvoiceAmt;
            }
            else
            {
                c.InvoiceAmt = 0;
            }
            InvoiceThisWeek = x.TryGetElementValue("InvoiceThisWeek", "false");
            if (bool.TryParse(InvoiceThisWeek, out blnInvoiceThisWeek))
            {
                c.InvoiceThisWeek = blnInvoiceThisWeek;
            }
            else
            {
                c.InvoiceThisWeek = false;
            }
            c.Terms = x.TryGetElementValue("Terms", "");
            InvoiceNo = x.TryGetElementValue("InvoiceNo", "0");
            if (int.TryParse(InvoiceNo, out intInvoiceNo))
            {
                c.InvoiceNo = intInvoiceNo;
            }
            else
            {
                c.InvoiceNo = 0;
            }
            InvoiceDate = x.TryGetElementValue("InvoiceDate", "01/01/1900");
            if (DateTime.TryParse(InvoiceDate, out dtInvoiceDate))
            {
                c.InvoiceDate = dtInvoiceDate;
            }
            else
            {
                c.InvoiceDate = DateTime.Parse("01/01/1900");
            }
            return c;
        }

        public static Customer GetCustomer(int CustomerId)
        {
            try
            {
                if (File.Exists(Globals.dataFilePath))
                {
                    XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                    XElement customer = xmlDoc.Descendants("customer").FirstOrDefault(c => c.Element("Id").Value == CustomerId.ToString());
                    if (customer != null)
                    {
                        Customer returnCustomer;
                        returnCustomer = PopulateCustomerInstance(customer);
                        return returnCustomer;
                    }
                    else
                    {
                        Exception ex = new Exception("Customer no longer exists on the database.");
                        Exceptions.HandleException(ex, "CustomerDAL_DeleteCustomer", true);
                        return null;
                    }
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                    Exceptions.HandleException(ex, "CustomerDAL_DeleteCustomer", true);
                    return null;
                }
            }
            catch (Exception ex)
            {
                Exceptions.HandleException(ex, "CustomerDAL_GetCustomer", true);
                return null;
            }
        }

        public static Customer GetCustomer(string AccountNumber, bool ShowError = false)
        {
            try
            {
                if (File.Exists(Globals.dataFilePath))
                {
                    XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                    XElement customer = xmlDoc.Descendants("customer").FirstOrDefault(c => c.Element("AccountNumber").Value == AccountNumber);
                    if (customer != null)
                    {
                        Customer returnCustomer;
                        returnCustomer = PopulateCustomerInstance(customer);
                        return returnCustomer;
                    }
                    else
                    {
                        Exception ex = new Exception("Customer no longer exists on the database.");
                        Exceptions.HandleException(ex, "CustomerDAL_DeleteCustomer", true);
                        return null;
                    }
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                    Exceptions.HandleException(ex, "CustomerDAL_DeleteCustomer", true);
                    return null;
                }
            }
            catch (Exception ex)
            {
                Exceptions.HandleException(ex, "CustomerDAL_GetCustomer", true);
                return null;
            }
        }

        public static bool DeleteCustomer(int CustomerId)
        {
            try
            {
                if (File.Exists(Globals.dataFilePath))
                {
                    XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                    XElement customer = xmlDoc.Descendants("customer").FirstOrDefault(c => c.Element("Id").Value == CustomerId.ToString());
                    if (customer != null)
                    {
                        customer.Remove();
                        xmlDoc.Save(Globals.dataFilePath);
                        return true;
                    }
                    else
                    {
                        Exception ex = new Exception("Customer no longer exists on the database.");
                        Exceptions.HandleException(ex, "CustomerDAL_DeleteCustomer", true);
                        return false;
                    }
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                    Exceptions.HandleException(ex, "CustomerDAL_DeleteCustomer", true);
                    return false;
                }
            }
            catch (Exception  ex1)
            {
                Exceptions.HandleException(ex1, "CustomerDAL_DeleteCustomer", true);
                return false;
            }
        }

        private static bool CheckDuplicate(int customerId, string AccountNumber)
        {
            try
            {
                if (File.Exists(Globals.dataFilePath))
                {
                    XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                    bool Dup = xmlDoc.Descendants("customer").Any
                        (c => c.Element("Id").Value != customerId.ToString() && c.Element("AccountNumber").Value == AccountNumber);
                    return Dup;
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                    Exceptions.HandleException(ex, "CustomerDAL_CheckDuplicate", true);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.HandleException(ex, "CustomerDAL_CheckDuplicate", true);
                return false;
            }
        }

        public static bool UpdateCustomer(Customer customer)
        {
            try
            {
                if (CheckDuplicate(customer.Id, customer.AccountNumber))
                {
                    Exception ex = new Exception("Customer Code already exists in the database. Customer codes must be unique.");
                    Exceptions.HandleException(ex, "CustomerDAL_UpdateCustomer", true);
                    return false;
                }

                if (File.Exists(Globals.dataFilePath))
                {
                    XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                    XElement x = xmlDoc.Descendants("customer").FirstOrDefault(c => c.Element("Id").Value == customer.Id.ToString());
                    if (x != null)
                    {
                        x.Remove();
                        x = new XElement("customer",
                            new XElement("Id", customer.Id),
                            new XElement("AccountNumber", customer.AccountNumber),
                            new XElement("Name", customer.Name),
                            new XElement("Address1", customer.Address1 ?? ""),
                            new XElement("Address2", customer.Address2 ?? ""),
                            new XElement("Town", customer.Town ?? ""),
                            new XElement("PostCode", customer.PostCode ?? ""),
                            new XElement("InvoiceOffset", customer.InvoiceOffset.ToString()),
                            new XElement("InvoiceText", customer.InvoiceText ?? ""),
                            new XElement("OutstandingBalance", customer.OutstandingBalance.ToString()),
                            new XElement("InvoiceAmt", customer.InvoiceAmt.ToString()),
                            new XElement("InvoiceThisWeek", customer.InvoiceThisWeek == true ? "True" : "False"),
                            new XElement("Terms", customer.Terms ?? ""),
                            new XElement("InvoiceNo", customer.InvoiceNo.ToString()),
                            new XElement("InvoiceDate", customer.InvoiceDate.ToShortDateString()));
                        xmlDoc.Root.Add(x);
                        xmlDoc.Save(Globals.dataFilePath);
                        return true;
                    }
                    else
                    {
                        Exception ex = new Exception("Customer no longer exists on the database.");
                        Exceptions.HandleException(ex, "CustomerDAL_UpdateCustomer", true);
                        return false;
                    }
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                    Exceptions.HandleException(ex, "CustomerDAL_UpdateCustomer", true);
                    return false;
                }
            }
            catch (Exception ex1)
            {
                Exceptions.HandleException(ex1, "CustomerDAL_UpdateCustomer", true);
                return false;
            }

        }

        public static bool AddCustomer(Customer customer)
        {
            try
            {
                if (CheckDuplicate(customer.Id, customer.AccountNumber))
                {
                    Exception ex = new Exception("Customer Code already exists in the database. Customer codes must be unique.");
                    Exceptions.HandleException(ex, "CustomerDAL_AddCustomer", true);
                    return false;
                }
                if (File.Exists(Globals.dataFilePath))
                {
                    XDocument xmlDoc = XDocument.Load(Globals.dataFilePath);
                    Properties.Settings.Default.MaxCustomerId += 1;
                    Properties.Settings.Default.Save();
                    int customerId = Properties.Settings.Default.MaxCustomerId;
                    
                    XElement x = new XElement("customer",
                                 new XElement("Id", customerId),
                                 new XElement("AccountNumber", customer.AccountNumber),
                                 new XElement("Name", customer.Name),
                                 new XElement("Address1", customer.Address1 ?? ""),
                                 new XElement("Address2", customer.Address2 ?? ""),
                                 new XElement("Town", customer.Town ?? ""),
                                 new XElement("PostCode", customer.PostCode ?? ""),
                                 new XElement("InvoiceOffset", customer.InvoiceOffset.ToString()),
                                 new XElement("InvoiceText", customer.InvoiceText ?? ""),
                                 new XElement("OutstandingBalance", customer.OutstandingBalance.ToString()),
                                 new XElement("InvoiceAmt", customer.InvoiceAmt.ToString()),
                                 new XElement("InvoiceThisWeek", customer.InvoiceThisWeek == true ? "True" : "False"),
                                 new XElement("Terms", customer.Terms ?? ""),
                                 new XElement("InvoiceNo", customer.InvoiceNo.ToString()),
                                 new XElement("InvoiceDate", customer.InvoiceDate.ToShortDateString()));
                    xmlDoc.Root.Add(x);
                    xmlDoc.Save(Globals.dataFilePath);
                    return true;
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist. Please restore from backup.");
                    Exceptions.HandleException(ex, "CustomerDAL_AddCustomer", true);
                    return false;
                }
            }
            catch (Exception ex1)
            {
                Exceptions.HandleException(ex1, "CustomerDAL_AddCustomer", true);
                return false;
            }

        }
    }
}
