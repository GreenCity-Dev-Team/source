﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZIPS.Data
{
    class Invoice
    {
        public string Title { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
        public int InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceText { get; set; }
        public decimal OutstandingBalance { get; set; }
        public decimal InvoiceAmt { get; set; }
        public decimal VAT { get; set; }
        public decimal Gross { get; set; }
        public string Terms { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyTelNos { get; set; }
        public string VATRegNo { get; set; }
    }
}
