﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;

namespace ZIPS.Classes
{
    class DataLayer
    {
        private DataSet m_data;

        private String m_filename;
        public string Filename
        {
            get { return m_filename; }
            set { m_filename = value; }
        }
        

        public DataLayer(string fileName)
        {
            m_data = new DataSet();
            try
            {
                m_data.ReadXml(fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            m_filename = fileName;
        }

        public DataTable Table(string tableName) { 
            {
                return m_data.Tables[tableName];
            }
        }

        public void Write()
        {
            m_data.WriteXml(m_filename);
        }
    }
}
