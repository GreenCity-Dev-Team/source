﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace ZIPS.Classes
{
    /// <summary>
    /// Class that holds details of how certain form fields should be validated
    /// </summary>
    public class clsFieldValidation
    {
        /// <summary>
        /// Data types to validate
        /// </summary>
        public enum InputTypes { Text, Number, DateTime }

        private Control _controlToValidate;

        /// <summary>
        /// Control on form to validate
        /// </summary>
        public Control ControlToValidate
        {
            get { return _controlToValidate; }
            set { _controlToValidate = value; }
        }

        private InputTypes _inputType;

        /// <summary>
        /// Input type (text, number or datetime) expected in the control
        /// </summary>
        public InputTypes InputType
        {
            get { return _inputType; }
            set { _inputType = value; }
        }

        private bool _required;

        /// <summary>
        /// Set to true if an entry in this control is mandatory
        /// </summary>
        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        /// <summary>
        /// Field Validation constructor
        /// </summary>
        /// <param name="controlToValidate">Control on the form to validate</param>
        /// <param name="inputType">Type of input expected in the control (from enum <c>clsFieldValidation.InputTypes</c></param>
        /// <param name="required">Set to true if an entry in this control is mandatory</param>
        public clsFieldValidation(Control controlToValidate, InputTypes inputType, bool required)
        {
            _controlToValidate = controlToValidate;
            _inputType = inputType;
            _required = required;
        }
    }
}
