﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

namespace ZIPS.Classes
{
    /// <summary>
    /// Class to handle exceptions in the application
    /// </summary>
    public static class Exceptions
    {
        /// <summary>
        /// Generic function to handle exceptions in a consistent manner
        /// - Writes to the application event log so Event Viewer can be used to see exceptions
        /// - Also creates an error object which will be written to the Error table
        /// </summary>
        /// <param name="ex1">Exception raised by caller</param>
        /// <param name="location">Code block where the exception was raised</param>
        /// <param name="showMessage">Set to true if an error message should be displayed to the user in a messagebox</param>
        public static void HandleException(Exception ex1, string location, bool showMessage)
        {
            WriteToEventLog(ex1.Message + "\n\n" + "Location: " + location, Application.ProductName,
                EventLogEntryType.Error, Globals.LogType.Application);

            if (showMessage)
            {
                try
                {
                    MessageBox.Show(ex1.Message + "\nLocation: " + location, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                }
            }

        }

        /// <summary>
        /// Write Event Log
        /// </summary>
        /// <param name="entry">Entry to be displayed in Event Log</param>
        /// <param name="appName">Application Name</param>
        /// <param name="eventType">What type of event you wish to log</param>
        /// <param name="logLocation">Place event in what location</param>
        public static void WriteToEventLog(string entry, string appName, EventLogEntryType eventType, Globals.LogType logLocation)
        {
            EventLog eventLog = new EventLog();
            string logName;

            switch (logLocation)
            {
                case Globals.LogType.Application:
                    logName = "Application";
                    break;
                case Globals.LogType.System:
                    logName = "System";
                    break;
                default:
                    logName = "Application";
                    break;
            }

            try
            {
                if (!EventLog.SourceExists(appName))
                {
                    EventLog.CreateEventSource(appName, logName);
                }
                eventLog.Source = appName;
                eventLog.WriteEntry(entry, eventType);
            }
            catch
            {
            }
        }

    }

}
