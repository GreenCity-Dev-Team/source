﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZIPS.Classes
{
    class CSVExport
    {
        /// <summary>
        /// List of fields to be output to CSV file
        /// </summary>
        List<string> fields = new List<string>();

        /// <summary>
        /// String for the first row of the export
        /// </summary>
        public string addTitle { get; set; }

        /// <summary>
        /// List of rows to add to the CSV file
        /// </summary>
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> currentRow
        {
            get
            {
                return rows[rows.Count - 1];
            }
        }

        public object this[string field]
        {
            set
            {
                if (!fields.Contains(field)) fields.Add(field);
                currentRow[field] = value;
            }
        }

        /// <summary>
        /// Add new row to CSV file
        /// </summary>
        public void AddRow()
        {
            rows.Add(new Dictionary<string, object>());
        }

        /// <summary>
        /// Converts "value" to a value that will ensure the CSV file does not confuse the value for more than one value
        /// i.e. by ensuring that a value with a comma in it is surrounded by speech marks and backslashes are amended to
        /// double backslashes. Also ensures dates are correctly formatted for CSV files
        /// </summary>
        /// <param name="value">Value to convert to CSV-friendly format</param>
        /// <returns></returns>
        string MakeValueCsvFriendly(object value)
        {
            if (value == null) return "";
            if (value is Nullable && ((INullable)value).IsNull) return "";
            if (value is DateTime)
            {
                if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
                    return ((DateTime)value).ToString("yyyy-MM-dd");
                return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
            }
            string output = value.ToString();
            if (output.Contains(",") || output.Contains("\""))
                output = '"' + output.Replace("\"", "\"\"") + '"';
            return output;

        }

        /// <summary>
        /// Create the content of the CSV file
        /// </summary>
        /// <returns>List of rows for writing to CSV file</returns>
        public string Export()
        {
            StringBuilder sb = new StringBuilder();

            // if there is a title
            if (!string.IsNullOrEmpty(addTitle))
            {
                // escape chars that would otherwise break the row / export
                char[] csvTokens = new[] { '\"', ',', '\n', '\r' };

                if (addTitle.IndexOfAny(csvTokens) >= 0)
                {
                    addTitle = "\"" + addTitle.Replace("\"", "\"\"") + "\"";
                }
                sb.Append(addTitle).Append(",");
                sb.AppendLine();
            }


            // The header
            foreach (string field in fields)
                sb.Append(field).Append(",");
            sb.AppendLine();

            // The rows
            foreach (Dictionary<string, object> row in rows)
            {
                foreach (string field in fields)
                    sb.Append(MakeValueCsvFriendly(row[field])).Append(",");
                sb.AppendLine();
            }

            return sb.ToString();
        }

        /// <summary>
        /// Export the CSV content to file
        /// </summary>
        /// <param name="path">Path of file to write CSV content to</param>
        public void ExportToFile(string path)
        {
            File.WriteAllText(path, Export(), Encoding.GetEncoding("Windows-1252"));
        }

        /// <summary>
        /// Export the CSV content as a stream of bytes
        /// </summary>
        /// <returns></returns>
        public byte[] ExportToBytes()
        {
            return Encoding.UTF8.GetBytes(Export());
        }

    }
}
