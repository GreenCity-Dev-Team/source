﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace ZIPS.Classes
{
    public static class Functions
    {
        /// <summary>
        /// Applies standard format to button control
        /// </summary>
        /// <param name="pBtn">Button control</param>
        public static void FormatButton(Button pBtn)
        {
            pBtn.FlatAppearance.BorderColor = Properties.Settings.Default.CompanyColour;
            pBtn.BackColor = Properties.Settings.Default.FormBackground;
            pBtn.FlatAppearance.BorderSize = 1;
        }

        /// <summary>
        /// Applies standard format to DataGridView control
        /// </summary>
        /// <param name="dg">DataGridView control</param>
        public static void StyleDataGridView(DataGridView dg)
        {
            dg.EnableHeadersVisualStyles = false;
            dg.RowsDefaultCellStyle.BackColor = Properties.Settings.Default.DGRow1Colour;
            dg.RowsDefaultCellStyle.SelectionBackColor = Properties.Settings.Default.OptionChosenColour;
            dg.RowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dg.AlternatingRowsDefaultCellStyle.SelectionBackColor = Properties.Settings.Default.OptionChosenColour;
            dg.AlternatingRowsDefaultCellStyle.BackColor = Properties.Settings.Default.DGRow2Colour;
            dg.AlternatingRowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dg.AutoGenerateColumns = false;
            dg.ColumnHeadersDefaultCellStyle.BackColor = Properties.Settings.Default.CompanyColour;
            dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dg.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dg.ColumnHeadersDefaultCellStyle.Font = Properties.Settings.Default.DGHdrFont;
            dg.RowHeadersDefaultCellStyle.BackColor = Properties.Settings.Default.CompanyColour;
            dg.RowHeadersDefaultCellStyle.ForeColor = Color.White;
            dg.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dg.BackgroundColor = Properties.Settings.Default.FormBackground;
            dg.MultiSelect = false;
        }

        /// <summary>
        /// Returns version of software 
        /// </summary>
        /// <returns>Version of software</returns>
        public static string GetRunningVersion()
        {
            string version = Application.ProductVersion;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                version = ad.CurrentVersion.ToString();
            }
            return version;
        }

        /// <summary>
        /// Extension method to determine if an element in an XML tree exists. If it does return its value, otherwise return a default value
        /// </summary>
        /// <param name="parentEl"></param>
        /// <param name="elementName"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string TryGetElementValue(this XElement parentEl, string elementName, string defaultValue = null)
        {
            var foundEl = parentEl.Element(elementName);

            if (foundEl != null)
            {
                return foundEl.Value;
            }

            return defaultValue;
        }
    }
}
