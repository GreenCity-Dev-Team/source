﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ZIPS.Classes
{
    public static class Globals
    {
        /// <summary>
        /// Log type for writing system events
        /// </summary>
        public enum LogType { Application, System }

        /// <summary>
        /// Equivalent of VB.Net IsNumeric function
        /// </summary>
        /// <param name="expression">Expression to test</param>
        /// <returns>Returns true if expression is a string that can be converted to a numeric</returns>
        public static bool IsNumeric(object expression)
        {
            bool isNum;
            double retNum;
            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static string dataFilePath
        {
            get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ZIPS", "customers.xml"); }
        }
    }
}
